-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2016-10-14 11:01:23
-- 服务器版本： 5.6.26-log
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jjcms`
--

-- --------------------------------------------------------

--
-- 表的结构 `jj_admin_log`
--

DROP TABLE IF EXISTS `jj_admin_log`;
CREATE TABLE `jj_admin_log` (
  `id` int(11) NOT NULL,
  `log_time` int(10) NOT NULL DEFAULT '0' COMMENT '记录时间',
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户id',
  `log_info` varchar(255) NOT NULL DEFAULT '0' COMMENT '日志内容',
  `ip_address` varchar(16) NOT NULL DEFAULT '0.0.0.0' COMMENT 'ip地址',
  `route` varchar(255) NOT NULL COMMENT '路由'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台操作日志';

--
-- 转存表中的数据 `jj_admin_log`
--

INSERT INTO `jj_admin_log` (`id`, `log_time`, `user_id`, `log_info`, `ip_address`, `route`) VALUES
(1, 1474889902, 3, 'jjcms登录后台', '127.0.0.1', 'site/login'),
(2, 1474891057, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(3, 1474891059, 3, '查看admin_log模型的字段列表', '127.0.0.1', 'table/attribute'),
(4, 1474891690, 3, '新增操作日志 菜单', '127.0.0.1', 'admin/menu/create'),
(5, 1474892270, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(6, 1474939913, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(7, 1474939918, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(8, 1474942256, 3, '登录后台', '192.168.1.103', 'site/login'),
(9, 1474943684, 3, '分配路由', '127.0.0.1', 'admin/route/assign'),
(10, 1474943844, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(11, 1474943847, 3, '查看user模型的字段列表', '127.0.0.1', 'table/attribute'),
(12, 1474943886, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(13, 1474944004, 3, '新增picture模型', '127.0.0.1', 'table/create'),
(14, 1474944008, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(15, 1474944312, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(16, 1474944314, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(17, 1474944357, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(18, 1474959253, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(19, 1474961416, 3, '修改了配置记录', '127.0.0.1', 'config/update'),
(20, 1475135292, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(21, 1475135294, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(22, 1475136715, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(23, 1475142751, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(24, 1475142958, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(25, 1475911202, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(26, 1475912544, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(27, 1475912580, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(28, 1475912624, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(29, 1475913030, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(30, 1475913063, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(31, 1475913248, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(32, 1475913281, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(33, 1475913294, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(34, 1475913314, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(35, 1475913577, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(36, 1475913621, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(37, 1475913823, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(38, 1475913846, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(39, 1475913849, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(40, 1475914040, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(41, 1475914042, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(42, 1475914310, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(43, 1475914317, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(44, 1475914320, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(45, 1475914321, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(46, 1475914330, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(47, 1475914339, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(48, 1475914344, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(49, 1475914346, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(50, 1475914454, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(51, 1475914582, 3, '向picture表增加了path字段', '127.0.0.1', 'attribute/create'),
(52, 1475914582, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(53, 1475914597, 3, '向picture表增加了url字段', '127.0.0.1', 'attribute/create'),
(54, 1475914597, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(55, 1475914649, 3, '向picture表增加了status字段', '127.0.0.1', 'attribute/create'),
(56, 1475914649, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(57, 1475914668, 3, '向picture表增加了created_time字段', '127.0.0.1', 'attribute/create'),
(58, 1475914668, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(59, 1475914953, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(60, 1475915073, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(61, 1475915074, 3, '查看picture模型的字段列表', '127.0.0.1', 'table/attribute'),
(62, 1475975663, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(63, 1475975667, 3, '查看user模型的字段列表', '127.0.0.1', 'table/attribute'),
(64, 1475975688, 3, '向user表增加了face字段', '127.0.0.1', 'attribute/create'),
(65, 1475975688, 3, '查看user模型的字段列表', '127.0.0.1', 'table/attribute'),
(66, 1475988913, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(67, 1475989076, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(68, 1475989079, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(69, 1475989122, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(70, 1475989137, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(71, 1475989375, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(72, 1475989629, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(73, 1475995040, 3, '上传图片并更换头像', '127.0.0.1', 'site/face'),
(74, 1475995045, 3, '访问了模型列表', '127.0.0.1', 'table/index'),
(75, 1476338227, 3, '修改了密码', '127.0.0.1', 'site/reset-password'),
(76, 1476338231, 3, '退出登录', '127.0.0.1', 'site/logout'),
(77, 1476338246, 3, '登录后台', '127.0.0.1', 'site/login');

-- --------------------------------------------------------

--
-- 表的结构 `jj_attribute`
--

DROP TABLE IF EXISTS `jj_attribute`;
CREATE TABLE `jj_attribute` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL COMMENT '字段标识',
  `note` varchar(64) NOT NULL COMMENT '字段注释',
  `field` varchar(255) NOT NULL COMMENT '字段定义',
  `type` varchar(16) NOT NULL COMMENT '数据类型',
  `default_value` varchar(32) NOT NULL COMMENT '字段默认值',
  `model_id` int(10) UNSIGNED NOT NULL COMMENT '模型id',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `remark` varchar(255) NOT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模型字段表';

--
-- 转存表中的数据 `jj_attribute`
--

INSERT INTO `jj_attribute` (`id`, `name`, `note`, `field`, `type`, `default_value`, `model_id`, `create_time`, `update_time`, `remark`) VALUES
(1, 'username', '用户名', 'VARCHAR(255) NOT NULL', 'string', '', 3, 1474421381, 1474421381, ''),
(2, 'auth_key', '身份密钥', 'VARCHAR(32) NOT NULL', 'string', '', 3, 1474421989, 1474421989, ''),
(3, 'password_hash', '用户密码', 'VARCHAR(255) NOT NULL', 'string', '', 3, 1474422195, 1474422195, ''),
(4, 'password_reset_token', '密码重置密钥', 'VARCHAR(255) NOT NULL', 'string', '', 3, 1474438247, 1474438247, ''),
(5, 'email', '邮箱', 'VARCHAR(255) NOT NULL', 'string', '', 3, 1474438721, 1474438721, ''),
(6, 'status', '状态', 'SMALLINT(6) NOT NULL', 'integer', '10', 3, 1474438783, 1474438783, ''),
(7, 'created_at', '创建时间', 'INT(10) NOT NULL', 'integer', '0', 3, 1474438844, 1474438844, ''),
(8, 'updated_at', '更新时间', 'INT(10) NOT NULL', 'integer', '0', 3, 1474438991, 1474438991, ''),
(9, 'name', '模型标识', 'VARCHAR(45) NOT NULL', 'string', '', 5, 1474439335, 1474439335, ''),
(10, 'title', '模型名称', 'VARCHAR(32) NOT NULL', 'string', '', 5, 1474439404, 1474439404, ''),
(11, 'need_pk', '是否需要主键(0-不用1-要)', 'CHAR(1) NOT NULL', 'string', '1', 5, 1474439480, 1474439480, ''),
(12, 'engine_type', '数据库引擎', 'VARCHAR(16) NOT NULL', 'string', 'InnoDB', 5, 1474439561, 1474439561, ''),
(13, 'create_time', '创建时间', 'INT(10) NOT NULL', 'integer', '0', 5, 1474439609, 1474439609, ''),
(14, 'update_time', '更新时间', 'INT(10) NOT NULL', 'integer', '0', 5, 1474439710, 1474439710, ''),
(15, 'name', '菜单名称', 'VARCHAR(128) NOT NULL', 'string', '', 2, 1474439853, 1474439853, ''),
(16, 'parent', '上级菜单id', 'INT(11) NULL ', 'integer', 'NULL', 2, 1474439981, 1474439981, ''),
(17, 'route', '路由url', 'VARCHAR(256) NULL', 'string', 'NULL', 2, 1474440042, 1474440042, ''),
(18, 'order', '映射/排序', 'INT(11) NULL', 'integer', 'NULL', 2, 1474440097, 1474440097, ''),
(19, 'data', '数据', 'TEXT NULL', 'string', 'NULL', 2, 1474440153, 1474440153, ''),
(20, 'web_title', '网站名称', 'VARCHAR(16) NOT NULL', 'string', '', 6, 1474440676, 1474440676, ''),
(21, 'web_describe', '网站描述', 'VARCHAR(128) NOT NULL', 'string', '', 6, 1474440761, 1474440761, ''),
(22, 'web_keyword', '网站关键字', 'VARCHAR(128) NOT NULL', 'string', '', 6, 1474440856, 1474440856, ''),
(24, 'name', '字段标识', 'VARCHAR(45) NOT NULL', 'string', '', 1, 1474441074, 1474441074, ''),
(25, 'note', '字段注释', 'VARCHAR(64) NOT NULL', 'string', '', 1, 1474441130, 1474441130, ''),
(26, 'field', '字段定义', 'VARCHAR(255) NOT NULL', 'string', '', 1, 1474441190, 1474441190, ''),
(27, 'type', '数据类型', 'VARCHAR(16) NOT NULL', 'string', '', 1, 1474441241, 1474441241, ''),
(28, 'default_value', '字段默认值', 'VARCHAR(32) NOT NULL', 'string', '', 1, 1474441353, 1474441353, ''),
(29, 'model_id', '模型id', 'INT(10) NOT NULL', 'integer', '', 1, 1474441473, 1474441473, ''),
(30, 'create_time', '创建时间', 'INT(11) NOT NULL', 'integer', '0', 1, 1474441542, 1474441542, ''),
(31, 'update_time', '更新时间', 'INT(11) NOT NULL', 'integer', '0', 1, 1474441580, 1474441580, ''),
(32, 'remark', '备注', 'VARCHAR(255) NOT NULL', 'string', '', 1, 1474441625, 1474441625, ''),
(33, 'web_record', '网站备案号', 'VARCHAR(128) NOT NULL', 'string', '', 6, 1474534195, 1474535164, ''),
(35, 'log_time', '记录时间', 'INT(10) NOT NULL', 'integer', '0', 9, 1474887290, 1474887290, ''),
(36, 'user_id', '用户id', 'INT(10) NOT NULL', 'integer', '', 9, 1474887762, 1474887762, ''),
(37, 'log_info', '日志内容', 'VARCHAR(255) NOT NULL', 'string', '', 9, 1474888040, 1474888040, ''),
(38, 'ip_address', 'ip地址', 'VARCHAR(16) NOT NULL', 'string', '0.0.0.0', 9, 1474888107, 1474888107, ''),
(41, 'route', '路由', 'VARCHAR(255) NOT NULL', 'string', '', 9, 1474889805, 1474889805, ''),
(42, 'path', '路径', 'VARCHAR(255) NOT NULL', 'string', '', 10, 1475914582, 1475914582, ''),
(43, 'url', '图片路径', 'VARCHAR(255) NOT NULL', 'string', '', 10, 1475914596, 1475914596, ''),
(44, 'status', '状态(0-停用1-启用)', 'CHAR(1) NOT NULL', 'string', '1', 10, 1475914649, 1475914649, ''),
(45, 'created_time', '创建时间', 'INT(10) NOT NULL', 'integer', '0', 10, 1475914667, 1475914667, ''),
(46, 'face', '头像', 'INT(10) NOT NULL', 'integer', '0', 3, 1475975687, 1475975687, '');

-- --------------------------------------------------------

--
-- 表的结构 `jj_auth_assignment`
--

DROP TABLE IF EXISTS `jj_auth_assignment`;
CREATE TABLE `jj_auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `jj_auth_assignment`
--

INSERT INTO `jj_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('guest', '4', 1474869125),
('超级管理员', '3', 1474263076);

-- --------------------------------------------------------

--
-- 表的结构 `jj_auth_item`
--

DROP TABLE IF EXISTS `jj_auth_item`;
CREATE TABLE `jj_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `jj_auth_item`
--

INSERT INTO `jj_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1474204288, 1474204288),
('/admin/*', 2, NULL, NULL, NULL, 1474203911, 1474203911),
('/admin/assignment/*', 2, NULL, NULL, NULL, 1474203381, 1474203381),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1474203378, 1474203378),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1474203372, 1474203372),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1474203379, 1474203379),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1474203375, 1474203375),
('/admin/default/*', 2, NULL, NULL, NULL, 1474203385, 1474203385),
('/admin/default/index', 2, NULL, NULL, NULL, 1474203384, 1474203384),
('/admin/menu/*', 2, NULL, NULL, NULL, 1474203390, 1474203390),
('/admin/menu/create', 2, NULL, NULL, NULL, 1474203843, 1474203843),
('/admin/menu/delete', 2, NULL, NULL, NULL, 1474203847, 1474203847),
('/admin/menu/index', 2, NULL, NULL, NULL, 1474203838, 1474203838),
('/admin/menu/update', 2, NULL, NULL, NULL, 1474203846, 1474203846),
('/admin/menu/view', 2, NULL, NULL, NULL, 1474203842, 1474203842),
('/admin/permission/*', 2, NULL, NULL, NULL, 1474203400, 1474203400),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1474203869, 1474203869),
('/admin/permission/create', 2, NULL, NULL, NULL, 1474203864, 1474203864),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1474203868, 1474203868),
('/admin/permission/index', 2, NULL, NULL, NULL, 1474203861, 1474203861),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1474203872, 1474203872),
('/admin/permission/update', 2, NULL, NULL, NULL, 1474203866, 1474203866),
('/admin/permission/view', 2, NULL, NULL, NULL, 1474203863, 1474203863),
('/admin/role/*', 2, NULL, NULL, NULL, 1474203403, 1474203403),
('/admin/role/assign', 2, NULL, NULL, NULL, 1474203881, 1474203881),
('/admin/role/create', 2, NULL, NULL, NULL, 1474203876, 1474203876),
('/admin/role/delete', 2, NULL, NULL, NULL, 1474203879, 1474203879),
('/admin/role/index', 2, NULL, NULL, NULL, 1474203873, 1474203873),
('/admin/role/remove', 2, NULL, NULL, NULL, 1474203883, 1474203883),
('/admin/role/update', 2, NULL, NULL, NULL, 1474203878, 1474203878),
('/admin/role/view', 2, NULL, NULL, NULL, 1474203875, 1474203875),
('/admin/route/*', 2, NULL, NULL, NULL, 1474203407, 1474203407),
('/admin/route/assign', 2, NULL, NULL, NULL, 1474203888, 1474203888),
('/admin/route/create', 2, NULL, NULL, NULL, 1474203886, 1474203886),
('/admin/route/index', 2, NULL, NULL, NULL, 1474203884, 1474203884),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1474203892, 1474203892),
('/admin/route/remove', 2, NULL, NULL, NULL, 1474203890, 1474203890),
('/admin/rule/*', 2, NULL, NULL, NULL, 1474203410, 1474203410),
('/admin/rule/create', 2, NULL, NULL, NULL, 1474203896, 1474203896),
('/admin/rule/delete', 2, NULL, NULL, NULL, 1474203899, 1474203899),
('/admin/rule/index', 2, NULL, NULL, NULL, 1474203893, 1474203893),
('/admin/rule/update', 2, NULL, NULL, NULL, 1474203898, 1474203898),
('/admin/rule/view', 2, NULL, NULL, NULL, 1474203895, 1474203895),
('/admin/user/*', 2, NULL, NULL, NULL, 1474203414, 1474203414),
('/admin/user/activate', 2, NULL, NULL, NULL, 1474203910, 1474203910),
('/admin/user/change-password', 2, NULL, NULL, NULL, 1474203908, 1474203908),
('/admin/user/delete', 2, NULL, NULL, NULL, 1474203528, 1474203528),
('/admin/user/index', 2, NULL, NULL, NULL, 1474203524, 1474203524),
('/admin/user/login', 2, NULL, NULL, NULL, 1474203901, 1474203901),
('/admin/user/logout', 2, NULL, NULL, NULL, 1474203902, 1474203902),
('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1474203905, 1474203905),
('/admin/user/reset-password', 2, NULL, NULL, NULL, 1474203907, 1474203907),
('/admin/user/signup', 2, NULL, NULL, NULL, 1474203904, 1474203904),
('/admin/user/view', 2, NULL, NULL, NULL, 1474203526, 1474203526),
('/adminlog/*', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/adminlog/create', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/adminlog/delete', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/adminlog/index', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/adminlog/update', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/adminlog/view', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/attribute/*', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/attribute/create', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/attribute/delete', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/attribute/update', 2, NULL, NULL, NULL, 1474891236, 1474891236),
('/common/*', 2, NULL, NULL, NULL, 1474891237, 1474891237),
('/config/*', 2, NULL, NULL, NULL, 1474348892, 1474348892),
('/config/create', 2, NULL, NULL, NULL, 1474348892, 1474348892),
('/config/delete', 2, NULL, NULL, NULL, 1474348892, 1474348892),
('/config/index', 2, NULL, NULL, NULL, 1474348892, 1474348892),
('/config/update', 2, NULL, NULL, NULL, 1474348892, 1474348892),
('/config/view', 2, NULL, NULL, NULL, 1474348892, 1474348892),
('/debug/default/db-explain', 2, NULL, NULL, NULL, 1474203913, 1474203913),
('/debug/default/index', 2, NULL, NULL, NULL, 1474203914, 1474203914),
('/gii/*', 2, NULL, NULL, NULL, 1474203928, 1474203928),
('/gii/default/*', 2, NULL, NULL, NULL, 1474203924, 1474203924),
('/gii/default/action', 2, NULL, NULL, NULL, 1474203924, 1474203924),
('/gii/default/diff', 2, NULL, NULL, NULL, 1474203924, 1474203924),
('/gii/default/index', 2, NULL, NULL, NULL, 1474203919, 1474203919),
('/gii/default/preview', 2, NULL, NULL, NULL, 1474203924, 1474203924),
('/gii/default/view', 2, NULL, NULL, NULL, 1474203924, 1474203924),
('/site/*', 2, NULL, NULL, NULL, 1474203420, 1474203420),
('/site/clear', 2, NULL, NULL, NULL, 1474203929, 1474203929),
('/site/error', 2, NULL, NULL, NULL, 1474203928, 1474203928),
('/site/face', 2, NULL, NULL, NULL, 1474943684, 1474943684),
('/site/index', 2, NULL, NULL, NULL, 1474203928, 1474203928),
('/site/login', 2, NULL, NULL, NULL, 1474203929, 1474203929),
('/site/logout', 2, NULL, NULL, NULL, 1474203929, 1474203929),
('/site/reset-password', 2, NULL, NULL, NULL, 1474203929, 1474203929),
('/table/*', 2, NULL, NULL, NULL, 1474363892, 1474363892),
('/table/attribute', 2, NULL, NULL, NULL, 1474891237, 1474891237),
('/table/create', 2, NULL, NULL, NULL, 1474363892, 1474363892),
('/table/delete', 2, NULL, NULL, NULL, 1474363892, 1474363892),
('/table/index', 2, NULL, NULL, NULL, 1474363892, 1474363892),
('/table/update', 2, NULL, NULL, NULL, 1474363892, 1474363892),
('/table/view', 2, NULL, NULL, NULL, 1474363892, 1474363892),
('guest', 2, '游客', NULL, NULL, 1474869105, 1474869105),
('root', 2, '超级管理员', NULL, NULL, 1474203461, 1476010563),
('超级管理员', 1, '超级管理员', NULL, NULL, 1474204321, 1474204321);

-- --------------------------------------------------------

--
-- 表的结构 `jj_auth_item_child`
--

DROP TABLE IF EXISTS `jj_auth_item_child`;
CREATE TABLE `jj_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `jj_auth_item_child`
--

INSERT INTO `jj_auth_item_child` (`parent`, `child`) VALUES
('root', '/*'),
('guest', '/site/index'),
('超级管理员', 'root');

-- --------------------------------------------------------

--
-- 表的结构 `jj_auth_rule`
--

DROP TABLE IF EXISTS `jj_auth_rule`;
CREATE TABLE `jj_auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `jj_config`
--

DROP TABLE IF EXISTS `jj_config`;
CREATE TABLE `jj_config` (
  `id` int(10) NOT NULL,
  `web_title` varchar(16) NOT NULL COMMENT '网站名称',
  `web_describe` varchar(128) NOT NULL COMMENT '网站描述',
  `web_keyword` varchar(128) NOT NULL COMMENT '网站关键字',
  `web_record` varchar(128) NOT NULL DEFAULT '' COMMENT '网站备案号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站配置内容表';

--
-- 转存表中的数据 `jj_config`
--

INSERT INTO `jj_config` (`id`, `web_title`, `web_describe`, `web_keyword`, `web_record`) VALUES
(1, 'jjCMS', '这是一款基于AdminLTE2.3所搭建的通用内容管理后台', 'yii2,jjcms,cms,php管理后台', '');

-- --------------------------------------------------------

--
-- 表的结构 `jj_menu`
--

DROP TABLE IF EXISTS `jj_menu`;
CREATE TABLE `jj_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `jj_menu`
--

INSERT INTO `jj_menu` (`id`, `name`, `parent`, `route`, `order`, `data`) VALUES
(2, '控制台', NULL, '/site/index', 1, NULL),
(3, '菜单管理', NULL, '/admin/menu/index', 2, NULL),
(5, '权限管理', NULL, '/admin/default/index', 3, NULL),
(6, '权限控制', 5, '/admin/default/index', NULL, NULL),
(7, '路由', 6, '/admin/route/index', NULL, NULL),
(8, '权限', 6, '/admin/permission/index', 2, NULL),
(9, '角色', 6, '/admin/role/index', 3, NULL),
(10, '分配', 6, '/admin/assignment/index', 4, NULL),
(11, '后台用户', 5, '/admin/user/index', 2, '{"icon": "fa fa-user", "visible": false}'),
(12, '系统设置', NULL, '/config/index', 4, NULL),
(13, '网站设置', 12, '/config/index', 1, NULL),
(14, '用户列表', 11, '/admin/user/index', 1, NULL),
(15, '注册用户', 11, '/admin/user/signup', 2, NULL),
(16, '模型管理', 12, '/table/index', 2, NULL),
(17, '操作日志', 11, '/adminlog/index', 3, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `jj_model`
--

DROP TABLE IF EXISTS `jj_model`;
CREATE TABLE `jj_model` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL COMMENT '模型标识',
  `title` varchar(32) NOT NULL COMMENT '模型名称',
  `need_pk` char(1) NOT NULL DEFAULT '1' COMMENT '是否需要主键(0-不用1-要)',
  `engine_type` varchar(16) NOT NULL DEFAULT 'InnoDB' COMMENT '数据库引擎',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模型表';

--
-- 转存表中的数据 `jj_model`
--

INSERT INTO `jj_model` (`id`, `name`, `title`, `need_pk`, `engine_type`, `create_time`, `update_time`) VALUES
(1, 'attribute', '模型字段表', '1', 'InnoDB', 1474364536, 1474364536),
(2, 'menu', '', '1', 'InnoDB', 1474364566, 1474364566),
(3, 'user', '', '1', 'InnoDB', 1474363536, 1474363536),
(5, 'model', '模型表', '1', 'InnoDB', 1474364576, 1474364576),
(6, 'config', '网站配置内容表', '1', 'InnoDB', 1474364516, 1474374516),
(9, 'admin_log', '后台操作日志', '1', 'InnoDB', 1474887146, 1474887146),
(10, 'picture', '图片表', '1', 'MyISAM', 1474944004, 1474944004);

-- --------------------------------------------------------

--
-- 表的结构 `jj_picture`
--

DROP TABLE IF EXISTS `jj_picture`;
CREATE TABLE `jj_picture` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL COMMENT '路径',
  `url` varchar(255) NOT NULL COMMENT '图片路径',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '状态(0-停用1-启用)',
  `created_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='图片表';

--
-- 转存表中的数据 `jj_picture`
--

INSERT INTO `jj_picture` (`id`, `path`, `url`, `status`, `created_time`) VALUES
(1, './uploads/face/98ea29b0.jpg', '', '1', 1475995040);

-- --------------------------------------------------------

--
-- 表的结构 `jj_user`
--

DROP TABLE IF EXISTS `jj_user`;
CREATE TABLE `jj_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `face` int(10) NOT NULL DEFAULT '0' COMMENT '头像'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `jj_user`
--

INSERT INTO `jj_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `face`) VALUES
(3, 'jjcms', 'DUAxNo3fDmKLS0_lbLMiu-ATwAvOwryR', '$2y$13$fukZwbKQqZsCIfz.nsG6LebvMkwUrx5xOuMyab.qn1rvA6qapmUsG', 'b', '598571948@qq.com', 10, 1469369644, 1476338227, 1),
(4, 'test', '50Vyl79fkdOa8keQWQiGiRuYEz8Tjz8u', '$2y$13$oaPIjH6CN0VIdKdcaRMgfe11jQrSTKKSIpVzVDfc0CM1ZRsydtkHi', 'a', 'test@123.com', 10, 1474351061, 1474351061, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jj_admin_log`
--
ALTER TABLE `jj_admin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jj_attribute`
--
ALTER TABLE `jj_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jj_auth_assignment`
--
ALTER TABLE `jj_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `jj_auth_item`
--
ALTER TABLE `jj_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `jj_auth_item_child`
--
ALTER TABLE `jj_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `jj_auth_rule`
--
ALTER TABLE `jj_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `jj_config`
--
ALTER TABLE `jj_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jj_menu`
--
ALTER TABLE `jj_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `jj_model`
--
ALTER TABLE `jj_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jj_picture`
--
ALTER TABLE `jj_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jj_user`
--
ALTER TABLE `jj_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `jj_admin_log`
--
ALTER TABLE `jj_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- 使用表AUTO_INCREMENT `jj_attribute`
--
ALTER TABLE `jj_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- 使用表AUTO_INCREMENT `jj_config`
--
ALTER TABLE `jj_config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `jj_menu`
--
ALTER TABLE `jj_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- 使用表AUTO_INCREMENT `jj_model`
--
ALTER TABLE `jj_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- 使用表AUTO_INCREMENT `jj_picture`
--
ALTER TABLE `jj_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `jj_user`
--
ALTER TABLE `jj_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 限制导出的表
--

--
-- 限制表 `jj_auth_assignment`
--
ALTER TABLE `jj_auth_assignment`
  ADD CONSTRAINT `jj_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `jj_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 限制表 `jj_auth_item`
--
ALTER TABLE `jj_auth_item`
  ADD CONSTRAINT `jj_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `jj_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- 限制表 `jj_auth_item_child`
--
ALTER TABLE `jj_auth_item_child`
  ADD CONSTRAINT `jj_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `jj_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jj_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `jj_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 限制表 `jj_menu`
--
ALTER TABLE `jj_menu`
  ADD CONSTRAINT `jj_menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `jj_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
