<?php

/* 
 * 公共自定义函数方法
 */
namespace common\widgets;
use backend\models\Picture;
use common\models\Menu;
use common\models\User;
use yii\helpers\Url;
use Yii;
use yii\data\ActiveDataProvider;

class functions
{
    /**
     * 获取属性别名
     * @param1  $model      yii\db\ActivityRecord       活动记录模型
     * @param2  $attribute  array                       要获取别名的字段(空则获取所有字段的别名)
     * return  array
     */
    public static function getAttributeLabel($model,$attribute = [])
    {
        if(empty($attribute)){
            $attr = $model->getAttributes();
            foreach($attr as $k=>$v){
                $attribute[] = $k;
            }
        }

        foreach($attribute as $_v){
            $label[] = $model->getAttributeLabel($_v);
        }

        return !empty($label)? $label : false;
    }

    /**
     * 根据where检索，返回yii的数据结果集，用于GridView控件中
     * @param1  $model  yii\db\ActivityRecord       活动记录模型
     * @param2  $where  array                       yii检索条件
     */
    public static function getDataProvider($model,$where)
    {
        if(!$model || !$where){
            return false;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $model::find()->where($where),
            'pagination'=>[
                'pageSize' => Yii::$app->params['pageSize']
            ]
        ]);

        return $dataProvider;
    }

    /**
     * 根据一个表主键值集合数组，如['1','2']。 将返回制定表中的指定字段的值，以字符串或者数组方式返回
     * @param1  $model  yii\db\ActivityRecord       活动记录模型
     * @param2  $ids    array                       主键值集合
     * @param3  $field  string                      想要返回的表中字段
     * @param4  $type   string                      返回值类型(string or array)
     * return   array|string
     */
    public static function getFieldValByIds($model,$ids,$field,$type='string')
    {
        if(!is_array($ids) || ($type!='string'&&$type!='array') || !$field){
            return false;
        }

        $res = [];
        foreach($ids as $id){
            $info = $model::findOne($id);
            $res[] = $info->$field;
        }

        return $type==='string'? implode(',',$res) : $res;
    }

    /**
     * 将ActivityRecord模型中的一个或多个字段取出，返回的样式有四种：
     * ① return  [id]=value
     * ② return  [field] = value
     * ③ return  [] = value
     * ④ return  [value] = value
     * @param  $param           array
     * @param  $param[attr]     string | array
     * @param  $param[where]    array
     * @param  $param[type]     IN ('index','rela')
     * @param  $param[pk]       string
     * @param  $param[relaType] IN ('mix','pure','equal')
     */
    public static function RecordToArray($model,$param = ['attr'=>'','where'=>['>','id',0],'type'=>'index','pk'=>'id','relaType'=>'mix'])
    {
        //参数验证
        if(!$model || !$param || !isset($param['attr']) || !isset($param['where']) || !is_array($param['where'] || !is_string($param['relaType']))){
            return [];
        }

        $tmpArr1 = $tmpArr2 = [];
        $records = $model::find()->where($param['where'])->all();

        if($records){

            if(is_string($param['attr']) && $param['type'] == 'index'){
                foreach($records as $item){
                    $tmpArr1["{$item->$param['pk']}"] = $item->$param['attr'];
                }
                return $tmpArr1;            // [id] = value
            }

            if(is_array($param['attr']) && $param['type'] == 'rela'){

                foreach($records as $item){
                    for($i=0,$len = count($param['attr']); $i<$len; $i++){

                        switch($param['relaType']){
                            case 'mix':     // [field] = value
                                $tmpArr1["{$param['attr'][$i]}"] = $item->$param['attr'][$i];
                                break;

                            case 'pure':    //[] = value
                                $tmpArr1[] = $item->$param['attr'][$i];
                                break;

                            case 'equal':   //[value] = value
                                $tmpArr1["{$item->$param['attr'][$i]}"] = $item->$param['attr'][$i];
                                break;
                        }

                    }

                    $tmpArr2[] = $tmpArr1;
                }

                return $tmpArr2;
            }

        }
    }

   /*
    * N维数组去空值
    * @param1  $array   array   要去除空元素的数组
    * return array
    */
   public static function moveEmptyInArray($array)
   {
       if (is_array($array)) {
        foreach ( $array as $k => $v ) {
            if (empty($v)) unset($array[$k]);
            elseif (is_array($v)) {
                $array[$k] = self::moveEmptyInArray($v);
            }
        }
     }
        return $array;
   }
   
   /*
    * 将数组转化为指定的一维索引数组
    * @param1 $array  array  数组
    * @param2 $indexStr array 新索引数组的键值
    * return array
    */
   protected static function convertAssoc($array,$indexStr)
   {
       $new_arr = array();
       $len = count($indexStr);
       if($array && is_array($array)){
           foreach($array as $v){
               for($i=0;$i<$len;$i++){
                   $new_arr[$indexStr[$i]] = $v[$indexStr[$i]];
               }
           }
           
       }
       return $new_arr;
   }


    /**
     * 随机产生字符串
     * @param1  string  $length  返回的字符串长度，最多32位
     * return string;
     */
    public static function randStr($length=8)
    {
        if(!is_int($length) || $length>32 || $length<=0){
            return fasle;
        }
        $rand ='!@_(';
        $str = 'abcdefghijklmnopqrstuvwxyz1234567890';
        for($i=mt_rand(0,4),$len=mt_rand(5,strlen($str));$i<$len;$i++){
            $rand .= $str[$i];
        }

        $key = md5(time().$rand);
        $key = substr($key,0,$length);
        return $key;
    }
}



