<?php
namespace api\controllers;
use backend\models\ConfigModel;
use Yii;
use yii\filters\VerbFilter;
class ConfigController extends CommonController
{
    public $defaultAction = 'index';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions'=> [
                    'index'  => ['post'],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $action = Yii::$app->request->post('action');
        switch((string)$action){
            case 'lists':
                return $this->lists();
                break;

            default:
                return $this->Error('方法名错误~!');
                break;
        }
    }

    private function lists()
    {
        $info = [
            ['name'=> 'jj','age'=>22],
            ['name'=>'zhangsan','age'=>18]
        ];
        return $this->Success($info);
    }



}