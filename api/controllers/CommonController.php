<?php
namespace api\controllers;
use yii\rest\Controller;

class CommonController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;

        return $behaviors;
    }


    protected function Success($data)
    {
        $array = [
            'status'    => '1',
            'info'      => $data
        ];

        return $array;
    }

    protected function Error($data)
    {
        $array = [
            'status'    => '0',
            'info'      => $data
        ];

        return $array;
    }

    protected function Returns($data)
    {
        return $data;
    }
}