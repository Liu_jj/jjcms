<?php
/* author:jj  date:2016/09/21 */
namespace backend\controllers;

use backend\models\Table;
use Yii;
use yii\filters\VerbFilter;
use backend\models\Attribute;
use backend\models\Adminlog;

class AttributeController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 新增字段
     */
    public function actionCreate($model_id)
    {
        $request = Yii::$app->request;
        $tbPrefix = Yii::$app->params['tablePrefix'];
        $model = new Attribute();
        if($request->isPost){
            $post = $request->post();
            $data = $request->post()['Attribute'];
            $data['create_time'] = $data['update_time'] = time();
            $post['Attribute'] = $data;
            $modelName = Table::findOne($data['model_id']);

            try{
                //1.检查数据表是否存在
                $sql = 'SHOW TABLES LIKE "'.$tbPrefix.$modelName->name.'"';
                $res = Yii::$app->db->createCommand($sql)->execute();

                //如果表不存在则要创建一个新表
                if(!$res){
                    if($data['default_value']!=''){
                        $sql = 'CREATE TABLE '.$tbPrefix.$modelName->name." (
                    {$data['name']} {$data['field']} DEFAULT '{$data['default_value']}' COMMENT '{$data['note']}'
                    )ENGINE='".$modelName->engine_type."'";
                    }else{
                        $sql = 'CREATE TABLE '.$tbPrefix.$modelName->name." (
                    {$data['name']} {$data['field']} COMMENT '{$data['note']}'
                    )ENGINE='".$modelName->engine_type."'";
                    }

                    Yii::$app->db->createCommand($sql)->execute();
                    $model->load($post) && $model->save();
                    Adminlog::saveLog($this->route,"创建了{$modelName->name}数据表并新增了{$data['name']}字段");
                    return $this->redirect(['table/attribute','id'=>$model_id]);

                    //2.为数据表新增字段
                    if($data['default_value']!=''){
                        $sql = 'ALTER TABLE '.$tbPrefix.$modelName->name." ADD {$data['name']} {$data['field']} DEFAULT '{$data['default_value']}' COMMENT '{$data['note']}'";
                    }else{
                        $sql = 'ALTER TABLE '.$tbPrefix.$modelName->name." ADD {$data['name']} {$data['field']} COMMENT '{$data['note']}'";
                    }

                    Yii::$app->db->createCommand($sql)->execute();
                    $model->load($post) && $model->save();
                    Adminlog::saveLog($this->route,"向{$modelName->name}表增加了{$data['name']}字段");
                    return $this->redirect(['table/attribute','id'=>$model_id]);
                }
            }catch(\Exception $e){
                Yii::$app->session->setFlash('msg-attr',['status'=>'danger','mes'=>'新增字段出错!']);
                return $this->redirect(['table/attribute','id'=>$model_id]);
            }

        }else{
            return $this->render('create',[
                'model_id'=>$model_id,
                'model' => $model
            ]);
        }
    }

    /**
     * 编辑字段
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = Attribute::findModel($id);
        $tbModel = Table::findOne($model->model_id);

        if($request->isPost){
            $post = $request->post();
            $data = $post['Attribute'];
            $data['update_time'] = time();
            $post['Attribute'] = $data;
            $tbPrefix = Yii::$app->params['tablePrefix'];

            $sql = 'ALTER TABLE '.$tbPrefix.$tbModel->name.' CHANGE '.$model->name.' '.$data['name']." {$data['field']} DEFAULT '{$data['default_value']}' COMMENT '{$data['note']}'";
            try{
                Yii::$app->db->createCommand($sql)->execute();
            }catch(\Exception $e){
                Yii::$app->session->setFlash('msg-attr',['status'=>'danger','mes'=>'修改字段失败!']);
                return $this->redirect(['/table/attribute','id'=>$tbModel->id]);
            }

            $model->load($post) && $model->save();
            Adminlog::saveLog($this->route,"在{$tbModel->name}表中修改了字段");
            return $this->redirect(['/table/attribute','id'=>$tbModel->id]);
        }else{
            return $this->render('edit',[
                'model_id'=>$model->model_id,
                'model' => $model
            ]);
        }
    }

    /**
     * 删除字段
     */
    public function actionDelete($id)
    {
        $model = Attribute::findModel($id);
        $tableModel = Table::findOne($model->model_id);
        $tbPrefix = Yii::$app->params['tablePrefix'];

        $sql = 'ALTER TABLE '.$tbPrefix.$tableModel->name.' DROP COLUMN '.$model->name;
        try{
            Yii::$app->db->createCommand($sql)->execute();
        }catch(\Exception $e){
            Yii::$app->session->setFlash('msg-attr',['status'=>'danger','mes'=>'删除字段失败!']);
            return $this->redirect(['/table/attribute','id'=>$tableModel->id]);
        }
        Adminlog::saveLog($this->route,"在{$tableModel->name}表中删除了字段{$model->name}");
        $model->delete();
        return $this->redirect(['/table/attribute','id'=>$tableModel->id]);
    }


}