<?php

namespace backend\controllers;

use backend\models\Adminlog;
use backend\models\Attribute;
use Yii;
use backend\models\Table;
use backend\models\TableSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * author:jj
 * date:2016/09/20
 */
class TableController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Table models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //Adminlog::saveLog($this->route,'访问了模型列表');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Table model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Table model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Table();
        $request = Yii::$app->request;

        if ($request->isPost) {
            $post = $request->post();
            $data = $post['Table'];
            $data['create_time'] = $data['update_time'] = time();

            $post['Table'] = $data;

            //检查是否有相同的数据表
            $res = Table::find()->where(['name'=>$data['name']])->one();
            if($res){
                return $this->redirect(['create','flag'=>true,'model'=>$model]);
            }

            //如果需要自增主键，那么创建数据表，不需要则不创建实际数据表
            if($data['need_pk'] == '1'){
                $createSql = 'CREATE TABLE '.Yii::$app->params['tablePrefix'].$data['name'].'(';
                $createSql.= 'id int NOT NULL PRIMARY KEY AUTO_INCREMENT)ENGINE="'.$data['engine_type'].'" COMMENT="'.$data['title'].'"';

                try{
                    Yii::$app->db->createCommand($createSql)->execute();
                }catch(\Exception $e){
                    Yii::$app->session->setFlash('msg-tb',['status'=>'danger','mes'=>'模型创建失败!']);
                    return $this->redirect(['index']);
                }
            }
            $model->load($post) && $model->save();
            Adminlog::saveLog($this->route,"新增{$data['name']}模型");
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Table model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $tbPrefix = Yii::$app->params['tablePrefix'];

        if ($request->isPost) {
            $post = $request->post();
            $data = $post['Table'];
            $data['update_time'] = time();
            $post['Table'] = $data;

            try{
                //1.修改表名
                $sql = 'ALTER TABLE '.$tbPrefix.$model->name.' rename '.$tbPrefix.$data['name'];
                Yii::$app->db->createCommand($sql)->execute();

                //2.修改表注释
                $sql = 'ALTER TABLE '.$tbPrefix.$model->name.' COMMENT "'.$data['title'].'"';
                Yii::$app->db->createCommand($sql)->execute();

                //3.修改数据库引擎
                $sql = 'ALTER TABLE '.$tbPrefix.$model->name.' engine='.$data['engine_type'];
                Yii::$app->db->createCommand($sql)->execute();
            }catch(\Exception $e){
                Yii::$app->session->setFlash('msg-tb',['status'=>'danger','mes'=>'修改模型失败!']);
                return $this->redirect(['index']);
            }


            $model->load($post) && $model->save();
            Adminlog::saveLog($this->route,"修改了{$model->name}模型");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Table model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        //1.删除数据表
        try{
            $delSql = "DROP table ".Yii::$app->params['tablePrefix'].$model->name;
            Yii::$app->db->createCommand($delSql)->execute();
        }catch(\Exception $e){
            Yii::$app->session->setFlash('msg-tb',['status'=>'danger','mes'=>'删除模型失败!']);
            return $this->redirect(['index']);
        }

        //2.删除attribute表中相关记录
        Attribute::deleteAll(['model_id'=>$id]);
        Adminlog::saveLog($this->route,"删除{$model->name}模型");
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Table model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Table the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Table::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 字段列表
     * @param1 integer $id
     */
    public function actionAttribute($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => Attribute::find()->where(['model_id'=>$id]),
            'pagination'=>[
                'pageSize' => Yii::$app->params['pageSize']
            ]
        ]);
        Adminlog::saveLog($this->route,"查看{$model->name}模型的字段列表");
        return $this->render('../attribute/attribute',['dataProvider'=>$dataProvider,'model'=>$model,'modelName'=>$model->name]);
    }


}
