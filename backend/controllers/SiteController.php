<?php
namespace backend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Adminlog;
use common\widgets\Uploads;

/**
 * Site controller
 */
class SiteController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','clear','reset-password','face'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $sysInfo = [
                ['name'=> '操作系统', 'value'=>php_uname('s')],  //'value'=>php_uname('s').' '.php_uname('r').' '.php_uname('v')],
                ['name'=>'PHP版本', 'value'=>phpversion()],
                ['name'=>'Yii版本', 'value'=>Yii::getVersion()],
                ['name'=>'数据库', 'value'=>$this->getDbVersion()],
                ['name'=>'AdminLTE', 'value'=>'V2.3.6'],
            ];
        return $this->render('index',[
            'sysInfo'=>$sysInfo
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Adminlog::saveLog($this->route,'登录后台');
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Adminlog::saveLog($this->route,'退出登录');
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * 修改密码
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $security = Yii::$app->getSecurity();
        $user = Yii::$app->user;

        if($request->isPost){
            $data = $request->post();

            //1.验证原密码是否正确
            if(!$security->validatePassword($data['old_pwd'],$user->identity->password_hash)){
                echo json_encode(['status'=>'0','name'=>'old_pwd','content'=>'原密码错误~!']);
                exit;
            }

            //2.查看前后两次密码是否一致
            if($data['new_pwd'] !== $data['new_pwd2']){
                echo json_encode(['status'=>'0','name'=>'new_pwd2','content'=>'前后密码不一致~!']);
                exit;
            }

            $hash = $security->generatePasswordHash($data['new_pwd']);
            $record = User::findOne($user->id);
            $record->password_hash = $hash;
            $record->save();
            if($record){
                Adminlog::saveLog($this->route,'修改了密码');
                echo json_encode(['status'=>'1']);
                exit;
            }
        }else{
            return $this->render('reset-password');
        }
    }
    
    /**
     * 清除缓存
     */
    public function actionClear()
    {
        \Yii::$app->cache->flush();
        \Yii::$app->session->setFlash('clear','缓存清除成功!');
        Adminlog::saveLog($this->route,'清除缓存!');
        $this->redirect(['/site/index']);
    }
    
    /**
     * 获取数据库版本
     */
    private function getDbVersion()
    {
        $driverName = Yii::$app->db->driverName;
        if(strpos($driverName, 'mysql') !== false){
            $v = Yii::$app->db->createCommand('SELECT VERSION() AS v')->queryOne();
            $driverName = $driverName .'_' . $v['v'];
        }
        return $driverName;
    }

    public function actionFace()
    {
        if(Yii::$app->request->isPost){
            $file = $_FILES['face'];
            //上传头像
            $upload = new Uploads($file);
            $upload->set('path','./uploads/face');
            $upload->set('allowType',['jpg','png','gif']);
            $res = $upload->uploads();

            if($res !== false){
                //修改头像
                $user = User::findOne(Yii::$app->user->identity->id);
                $user->face = $res;
                $user->save();
                Yii::$app->session->setFlash('msg',['status'=>'success','mes'=>'头像更换成功!']);
                Adminlog::saveLog($this->route,'上传图片并更换头像');
                $this->redirect(['/site/index']);
            }else{
                $error = $upload->getError();
                Yii::$app->session->setFlash('msg',['status'=>'warning','mes'=>$error]);
                $this->redirect(['/site/face']);
            }
        }else{
            return $this->render('face');
        }
    }

}
