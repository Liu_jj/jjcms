<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AdminlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '操作日志';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adminlog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',

            [
                'attribute' => 'user_id',
                'value'     => function($data){
                    return \common\models\User::findOne($data->user_id)->username;
                }
            ],
            'route',
            'log_info',
            'ip_address',
            [
                'attribute' => 'log_time',
                'value'     => function($data){
                    return date('Y-m-d H:i:s',$data->log_time);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view}'
            ],
        ],
    ]); ?>
</div>
