<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Adminlog */

$this->title = $model->log_info;
$this->params['breadcrumbs'][] = ['label' => 'Adminlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adminlog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'log_time',
                'value' => date('Y-m-d H:i:s',$model->log_time)
            ],
            'user_id',
            'log_info',
            'ip_address',
            'route',
        ],
    ]) ?>

</div>
