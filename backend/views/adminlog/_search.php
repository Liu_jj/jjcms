<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AdminlogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adminlog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'log_time') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'log_info') ?>

    <?= $form->field($model, 'ip_address') ?>

    <?php // echo $form->field($model, 'route') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
