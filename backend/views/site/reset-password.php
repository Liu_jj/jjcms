<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
$this->title = '修改密码';
?>

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border"><h3 class="box-title"><?= $this->title; ?></h3> </div>
            <?= Html::beginForm(['site/reset-password'],'post',['class'=>'form-horizontal','id'=>'form1']); ?>
                <div class="form-group">
                    <?= Html::label('旧密码:','inputPassword3',['class'=>'col-sm-3 control-label']) ?>
                    <div class="col-sm-8">
                        <?= Html::passwordInput('old_pwd','',['class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::label('新密码:','inputPassword3',['class'=>'col-sm-3 control-label']) ?>
                    <div class="col-sm-8">
                        <?= Html::passwordInput('new_pwd','',['class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::label('再次输入:','inputPassword3',['class'=>'col-sm-3 control-label']) ?>
                    <div class="col-sm-8">
                        <?= Html::passwordInput('new_pwd2','',['class'=>'form-control']) ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9">
                        <label id="msg-info" class="control-label text-green">
                            <i class="fa fa-check"></i>密码修改成功
                        </label>
                    </div>
                    <?= Html::button('修改密码',['class'=>'btn btn-info','id'=>'sub-btn']) ?>
                </div>
            <?= Html::endForm(); ?>


        </div>
    </div>
    <div class="col-md-5"></div>
</div>


<script>
    <?php $this->beginBlock('js') ?>

        $('#msg-info').hide();
        //1.ajax提交表单
        $('#sub-btn').click(function(){
            var form_data = $('#form1').serialize();
            $.ajax({
                type    :   "post",
                dataType:   "json",
                url     :   "<?= Url::toRoute('site/reset-password') ?>",
                data    :   form_data,
                success : function(value){
                    if(value.status==0){
                        $("input[name='"+value.name+"']").attr({
                            'data-toggle' : 'popover',
                            'data-placement' : 'bottom',
                            'data-content'   : value.content
                        }).addClass('popover-show').popover('show');
                    }else{
                        $('#msg-info').show();
                    }
                }
            });
        });


    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js'],\yii\web\View::POS_LOAD); ?>
