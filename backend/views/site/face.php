<?php
use kartik\file\FileInput;
use yii\helpers\Html;
$this->title = '上传头像';
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">上传头像</h3>
    </div>
    <div class="panel-body">
        <div class="box-body">
            <?= Html::beginForm('face','post',['enctype'=>'multipart/form-data']) ?>
            <?=
                FileInput::widget([
                    'name'      => 'face',
                    'pluginOptions' => [
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => true,
                        'uploadLabel'=>'上传',
                        'browseLabel' => '选择',
                        'removeLabel' => '删除',
                    ],
                    //'options'   => ['multiple'=>true]
                ]);
            ?>
            <?= Html::endForm(); ?>
        </div>
    </div>
</div>