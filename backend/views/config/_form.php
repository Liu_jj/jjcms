<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'web_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'web_describe')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'web_keyword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'web_record')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '确定' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
