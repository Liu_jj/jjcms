<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ConfigModel */

$this->title = '新增设置';
$this->params['breadcrumbs'][] = ['label' => '网站设置', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
