<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '网站设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?php
          /*if(Helper::checkRoute('create')){
              echo Html::a('新增设置', ['create'], ['class' => 'btn btn-success']);
          }*/
        ?>
    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'web_title',
            'web_describe',
            'web_keyword',
            'web_record',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
