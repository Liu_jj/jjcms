<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Table */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form">

    <?php if(isset($flag)){ ?>
    <div class="alert alert-danger alert-dismissable" style="width:50%;">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">
            &times;
        </button>
        抱歉!  已经存在相同的模型标识
    </div>
    <?php } ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'need_pk')->dropDownList(['1'=>'需要','0'=>'不需要']) ?>

    <?= $form->field($model, 'engine_type')->dropDownList(['InnoDB'=>'InnoDB','MyISAM'=>'MyISAM','MEMORY'=>'MEMORY']) ?>

    <!--<?= $form->field($model, 'create_time')->textInput() ?>

    <?= $form->field($model, 'update_time')->textInput() ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '确定' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
