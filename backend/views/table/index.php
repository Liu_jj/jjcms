<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '数据表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('新增模型', ['create'], ['class' => 'btn btn-success']) ?> &nbsp;
    </p>

    <?php
    $mess = Yii::$app->session->getFlash('msg-tb');
    if($mess){
        $str = '<div class="alert alert-'.$mess['status'].' alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">
                            &times;
                        </button>
                        '.$mess['mes'].'
                        </div>';
        echo $str;
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'title',
            //'need_pk',
            'engine_type',
            [
                'attribute' => 'create_time',
                'value' => function($data){
                    return date('Y-m-d H:i:d',$data->create_time);
                }
            ],
            // 'update_time:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=> '操作',
                'template'=> '{view} {update} {delete} {attribute}',
                'buttons' => [
                    'attribute' => function($url,$model,$key){
                        return Html::a('<i class="fa fa-edit"></i> 字段管理',$url,['class'=>'btn btn-info btn-xs']);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
