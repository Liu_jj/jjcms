<?php
/*
  后台操作日志模型
*/
namespace backend\models;
use yii\db\ActiveRecord;
use Yii;

class Adminlog extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%admin_log}}";
    }

    public function attributeLabels()
    {
        return [
            'log_time'      => '记录时间',
            'user_id'       => '操作用户',
            'log_info'      => '日志内容',
            'ip_address'    => 'IP地址',
            'route'         => '路由地址'
        ];
    }

    public static function saveLog($route,$log_info)
    {
        $model = new self;
        $model->ip_address = Yii::$app->request->userIP;
        $model->log_time = time();
        $model->user_id = Yii::$app->user->identity->id;
        $model->log_info = $log_info;
        $model->route = $route;
        $model->save();
    }
}