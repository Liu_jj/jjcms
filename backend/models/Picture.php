<?php
namespace backend\models;
use yii\db\ActiveRecord;

class Picture extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%picture}}";
    }

    public function rules()
    {
        return [
            [['id'],'safe'],
            [['path','url','status'],'string'],
            [['created_time'],'integer']
        ];
    }
}