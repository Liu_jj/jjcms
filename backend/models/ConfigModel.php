<?php
/**
 *  网站设置模型
 */
namespace backend\models;
use yii\db\ActiveRecord;

class ConfigModel extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%config}}";
    }

    public function rules()
    {
        return [
            ['id','safe'],
            [['web_title','web_describe','web_keyword','web_record'],'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'web_title'     => '网站名称',
            'web_describe'  => '网站描述',
            'web_keyword'   => '关键字',
            'web_record'    => '备案号'
        ];
    }
}